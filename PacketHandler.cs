﻿/*
Savagedlight.Network
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight;
using Savagedlight.Extensions;
using Savagedlight.Network.Attributes;
using Savagedlight.Network.Exceptions;
using Savagedlight.Network.Packets;
using Savagedlight.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Savagedlight.Network
{
    /// <summary>
    /// Keeps track of valid packages,<br/>
    /// and provides an interface for deserializing packets.
    /// </summary>
    public class PacketHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// This member is only altered from instance constructor.<br/>
        /// It is therefore safe to not use locks on reads.
        /// </remarks>
        private Dictionary<UInt16, PacketInfo> packets = new Dictionary<UInt16, PacketInfo>();
        private Dictionary<Type, UInt16> definitionToId = new Dictionary<Type, UInt16>();

        public UInt16 GetPacketId(Type definition)
        {
            UInt16 id;
            if (!definitionToId.TryGetValue(definition, out id))
            {
                throw new KeyNotFoundException();
            }
            return id;
        }

        public PacketHandler(params Assembly[] scanAssemblies)
        {
            // Register my own stuff first.
            RegisterPackets(this.GetType().Assembly, true);

            foreach (Assembly assembly in scanAssemblies)
            {                
                RegisterPackets(assembly);
            }
        }

        public byte[] SerializePacket(Packet packet)
        {
            using (MemoryStream ms = new MemoryStream(1024))
            {
                using (SuperStream ss = new SuperStream(ms, Endianess.Little))
                {
                    StreamData.Serialize(packet, ss);
                    return ms.ToArray();
                }
            }
        }

        public Packet DeserializePacket(ushort packetId, byte[] data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }
            
            if (!this.IsValidPacketId(packetId))
            {
                throw new InvalidPacketIdException(packetId);
            }

            Type definition = this.packets[packetId].Definition;
            Packet packet;
            using (MemoryStream ms = new MemoryStream(data))
            {
                using (SuperStream ss = new SuperStream(ms, Endianess.Little)) 
                {
                    try
                    {
                        packet = (Packet)StreamData.Deserialize(definition, ss);
                    }
                    catch (Exception ex)
                    {
                        // Todo: Make a new exception t for this, detailing packet id and length.
                        throw new InvalidDataException("Malformed packet data.", ex);
                    }
                }
            }

            return packet;
        }

        #region Methods for registering packet definitions and types.
        private void RegisterPackets(Assembly assembly, bool allowReserved=false)
        {
            Type baseType = typeof(Packet);
            var types = from t in assembly.GetTypes()
                        where t != baseType
                        where baseType.IsAssignableFrom(t)
                        let attr = t.GetAttribute<PacketAttribute>()
                        select new KeyValuePair<Type, PacketAttribute>(t, attr);


            foreach (var kvp in types)
            {
                if (!allowReserved && kvp.Value.PacketId <= 100)
                {
                    throw new ReservedPacketIdException(kvp.Value.PacketId);
                }
                RegisterPacket(kvp.Key, kvp.Value);
            }
        }

        private void RegisterPacket(Type definition, PacketAttribute attr)
        {
            if (attr == null)
            {
                throw new MissingPacketAttributePacketDefinitionException(definition);
            }

            IsValidPacket(definition);

            if (this.packets.ContainsKey(attr.PacketId))
            {
                throw new AlreadyRegisteredPacketIdException(
                    attr.PacketId,
                    this.packets[attr.PacketId].Definition,
                    definition);
            }
            this.packets[attr.PacketId] = new PacketInfo(attr.PacketId, attr.Direction, definition);
            this.definitionToId[definition] = attr.PacketId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="definition"></param>
        /// <exception cref="Exception">Throws exception if packet format is not right.</exception>
        private static void IsValidPacket(Type definition)
        {
            // Test if t has valid constructors.
            bool haveArglessConstructor = false;
            var constructors = definition.GetConstructors(BindingFlags.Instance | BindingFlags.Public);
            foreach (var c in constructors)
            {
                var parameters = c.GetParameters();
                if (parameters.Length == 0)
                {
                    haveArglessConstructor = true;
                    continue;
                }
            }

            if (!haveArglessConstructor)
            {
                throw new MissingArglessConstructorPacketDefinitionException(definition);
            }
        }
        #endregion

        public bool IsValidPacketId(ushort packetId)
        {
            return this.packets.ContainsKey(packetId);
        }

        public bool IsValidPacketDirection(ushort packetId, PacketDirection packetDirection)
        {
            PacketInfo info;
            if (!this.packets.TryGetValue(packetId, out info))
            {
                throw new InvalidPacketIdException(packetId);
            }

            int allowed = (int)info.Direction;
            int dir = (int)packetDirection;
            return (allowed & dir) == dir;
        }
    }
}

